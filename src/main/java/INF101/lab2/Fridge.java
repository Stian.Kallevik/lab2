package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

        // ctrl + . for å autogenere metoder.


    // create new array list - remember that in the method you need to check for a max capacity/limit for the fridge
    
    ArrayList<FridgeItem> fridgeList = new ArrayList<>();
    //List<FridgeItem> expiredItemsList = new ArrayList<>();

    @Override // da kjøres metoden i denne filen
    public int nItemsInFridge() {
        // returns number of items in the fridge as an int
        // test OK

        return fridgeList.size();
    }

    @Override
    public int totalSize() {
        // Test oK
        return 20;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TEST OK

        // throw NoSuchElementException if the fridgeItem is not found.
        if (fridgeList.contains(item) == true ) {
            fridgeList.remove(item);
        } else {
            throw new NoSuchElementException(); 
        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        fridgeList.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> expiredItemsList = new ArrayList<>();
        for (int i = 0; i < fridgeList.size(); i++) {
            FridgeItem checkItem = fridgeList.get(i);

            if (checkItem.hasExpired() == true) {
                //fridgeList.remove(checkItem);
                expiredItemsList.add(checkItem);
            }
        }
        fridgeList.removeAll(expiredItemsList);
        return expiredItemsList;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // returns a boolean data type true = if there is more room in the fridge
        // Test OK
        
        if (fridgeList.size() < 20) {
            fridgeList.add(item);
            return true;
        } else {
            return false;
        }

    }
    


    
}
